<?php
return [
    'izytech_laravel_validation_required' => "Pacchetto richiesto. Installa izytech/laravel-validation ('composer require izytech/laravel-validation'), per favore.",
    'league_fractal_required'             => "Pacchetto richiesto. Installa league/fractal ('composer require league/fractal') (0.12.*), per favore."
];
