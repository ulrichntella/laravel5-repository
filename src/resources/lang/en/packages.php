<?php
return [
    'izytech_laravel_validation_required' => "Package required. Please install: 'composer require izytech/laravel-validation'",
    'league_fractal_required'             => "Package required. Please install: 'composer require league/fractal' (0.12.*)"
];
