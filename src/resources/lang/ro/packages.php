<?php
return [
    'izytech_laravel_validation_required' => "Pachet obligatoriu. Instalaţi izytech/laravel-validation ('composer require izytech/laravel-validation'), vă rog.",
    'league_fractal_required'             => "Pachet obligatoriu. Instalaţi league/fractal ('composer require league/fractal') (0.12.*), vă rog."
];
