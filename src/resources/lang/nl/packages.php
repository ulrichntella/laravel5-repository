<?php
return [
    'izytech_laravel_validation_required' => "Package vereist. Installeer: 'composer require izytech/laravel-validation'",
    'league_fractal_required'             => "Package vereist. Installeer: 'composer require league/fractal' (0.12.*)"
];
