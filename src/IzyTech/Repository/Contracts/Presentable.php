<?php
namespace IzyTech\Repository\Contracts;

/**
 * Interface Presentable
 * @package IzyTech\Repository\Contracts
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
interface Presentable
{
    /**
     * @param PresenterInterface $presenter
     *
     * @return mixed
     */
    public function setPresenter(PresenterInterface $presenter);

    /**
     * @return mixed
     */
    public function presenter();
}
