<?php
namespace IzyTech\Repository\Contracts;

/**
 * Interface CriteriaInterface
 * @package IzyTech\Repository\Contracts
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
interface CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository);
}
