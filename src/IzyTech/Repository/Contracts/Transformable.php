<?php
namespace IzyTech\Repository\Contracts;

/**
 * Interface Transformable
 * @package IzyTech\Repository\Contracts
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
interface Transformable
{
    /**
     * @return array
     */
    public function transform();
}
