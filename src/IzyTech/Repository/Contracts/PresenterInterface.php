<?php
namespace IzyTech\Repository\Contracts;

/**
 * Interface PresenterInterface
 * @package IzyTech\Repository\Contracts
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
interface PresenterInterface
{
    /**
     * Prepare data to present
     *
     * @param $data
     *
     * @return mixed
     */
    public function present($data);
}
