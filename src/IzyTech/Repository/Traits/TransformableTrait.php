<?php

namespace IzyTech\Repository\Traits;

/**
 * Class TransformableTrait
 * @package IzyTech\Repository\Traits
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
trait TransformableTrait
{
    /**
     * @return array
     */
    public function transform()
    {
        return $this->toArray();
    }
}
