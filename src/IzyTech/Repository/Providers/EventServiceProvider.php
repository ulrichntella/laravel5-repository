<?php
namespace IzyTech\Repository\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class EventServiceProvider
 * @package IzyTech\Repository\Providers
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
class EventServiceProvider extends ServiceProvider
{

    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'IzyTech\Repository\Events\RepositoryEntityCreated' => [
            'IzyTech\Repository\Listeners\CleanCacheRepository'
        ],
        'IzyTech\Repository\Events\RepositoryEntityUpdated' => [
            'IzyTech\Repository\Listeners\CleanCacheRepository'
        ],
        'IzyTech\Repository\Events\RepositoryEntityDeleted' => [
            'IzyTech\Repository\Listeners\CleanCacheRepository'
        ]
    ];

    /**
     * Register the application's event listeners.
     *
     * @return void
     */
    public function boot()
    {
        $events = app('events');

        foreach ($this->listen as $event => $listeners) {
            foreach ($listeners as $listener) {
                $events->listen($event, $listener);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function register()
    {
        //
    }

    /**
     * Get the events and handlers.
     *
     * @return array
     */
    public function listens()
    {
        return $this->listen;
    }
}
