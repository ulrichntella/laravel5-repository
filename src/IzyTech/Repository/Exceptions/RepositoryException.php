<?php
namespace IzyTech\Repository\Exceptions;

use Exception;

/**
 * Class RepositoryException
 * @package IzyTech\Repository\Exceptions
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
class RepositoryException extends Exception
{

}
