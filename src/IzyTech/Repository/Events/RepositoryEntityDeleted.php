<?php
namespace IzyTech\Repository\Events;

/**
 * Class RepositoryEntityDeleted
 * @package IzyTech\Repository\Events
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
class RepositoryEntityDeleted extends RepositoryEventBase
{
    /**
     * @var string
     */
    protected $action = "deleted";
}
