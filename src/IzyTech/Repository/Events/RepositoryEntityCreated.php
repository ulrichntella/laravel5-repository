<?php
namespace IzyTech\Repository\Events;

/**
 * Class RepositoryEntityCreated
 * @package IzyTech\Repository\Events
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
class RepositoryEntityCreated extends RepositoryEventBase
{
    /**
     * @var string
     */
    protected $action = "created";
}
