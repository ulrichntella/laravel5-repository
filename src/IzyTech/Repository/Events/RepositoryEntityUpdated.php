<?php
namespace IzyTech\Repository\Events;

/**
 * Class RepositoryEntityUpdated
 * @package IzyTech\Repository\Events
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
class RepositoryEntityUpdated extends RepositoryEventBase
{
    /**
     * @var string
     */
    protected $action = "updated";
}
