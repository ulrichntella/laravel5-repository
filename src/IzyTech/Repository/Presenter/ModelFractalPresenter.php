<?php
namespace IzyTech\Repository\Presenter;

use Exception;
use IzyTech\Repository\Transformer\ModelTransformer;

/**
 * Class ModelFractalPresenter
 * @package IzyTech\Repository\Presenter
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
class ModelFractalPresenter extends FractalPresenter
{

    /**
     * Transformer
     *
     * @return ModelTransformer
     * @throws Exception
     */
    public function getTransformer()
    {
        if (!class_exists('League\Fractal\Manager')) {
            throw new Exception("Package required. Please install: 'composer require league/fractal' (0.12.*)");
        }

        return new ModelTransformer();
    }
}
