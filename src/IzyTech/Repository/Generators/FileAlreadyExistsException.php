<?php
namespace IzyTech\Repository\Generators;

use Exception;

/**
 * Class FileAlreadyExistsException
 * @package IzyTech\Repository\Generators
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
class FileAlreadyExistsException extends Exception
{
}
