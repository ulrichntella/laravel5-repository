<?php

namespace IzyTech\Repository\CursorPaginate;

/**
 * Class Cursor
 * @package IzyTech\Repository\CursorPaginate
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
class Cursor
{
    protected $prev = null;
    protected $next = null;

    /**
     * Cursor constructor.
     *
     * @param null $prev
     * @param null $next
     */
    public function __construct($prev = null, $next = null): void  
    {
        $this->prev = $prev;
        $this->next = $next;
    }

    /**
     * @return bool
     */
    public function isPresent(): bool
    {
        return $this->isNext() || $this->isPrev();
    }

    /**
     * @return bool
     */
    public function isNext() : bool
    {
        return !is_null($this->next);
    }

    /**
     * @return bool
     */
    public function isPrev(): bool
    {
        return !is_null($this->prev);
    }

    /**
     * @return mixed
     */
    public function getPrevCursor(): mixed 
    {
        return $this->prev;
    }

    /**
     * @return mixed
     */
    public function getNextCursor(): mixed 
    {
        return $this->next;
    }
}
