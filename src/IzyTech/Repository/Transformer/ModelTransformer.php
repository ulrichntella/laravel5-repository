<?php namespace IzyTech\Repository\Transformer;

use IzyTech\Fractal\TransformerAbstract;
use IzyTech\Repository\Contracts\Transformable;

/**
 * Class ModelTransformer
 * @package IzyTech\Repository\Transformer
 * @author Ulrich Ntella <ulrichsoft2002@gmail.com>
 */
class ModelTransformer extends TransformerAbstract
{
    public function transform(Transformable $model)
    {
        return $model->transform();
    }
}
